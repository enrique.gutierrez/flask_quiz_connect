# Flask Quiz

This project containst he code for creating an Only connect project

# Install

Execute the `setup.sh` bash script to create a virtual environment and install dependencies from requirements
```sh
bash -c './setup.sh'
```

# Running

1. Activate the virtual environment created by the setup `.venv/bin/activate`
2. Run `python3 app.py`
3. Open your webrowser on `http://localhost:5000/`