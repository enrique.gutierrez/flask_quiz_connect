import os
from flask import Flask, render_template, request, url_for
import yaml

app = Flask(__name__)
base_dir = os.path.dirname(os.path.abspath(__file__))
config_file_path = 'config.yaml'
with open(f"{base_dir}/{config_file_path}") as f:
    config = yaml.safe_load(f)

# TODO: Add timer!!!


@app.route('/')
def home():
    return render_template('home.html', title="Home")


@app.route('/only-connect', methods=['POST', 'GET'])
def only_connect():
    questions = config['questions']['only_connect']
    if request.method == 'POST':
        return render_template('only_connect.html',
                               questions=questions,
                               title="Only Connect")
    else:
        return 'There was a problem executing that task'


@app.route('/only-connect-question', methods=['POST', 'GET'])
def only_connect_question():
    questions = config['questions']['only_connect']
    if request.method == 'POST':
        q = request.form['question']
        q_selected = questions[q]
        config['questions']['only_connect'][q].update({'ignore': True})
        return render_template('only_connect_question.html',
                               questions=q_selected,
                               title="Only Connect")
    else:
        return 'There was a problem executing that task'


@app.route('/sequence-connect', methods=['POST', 'GET'])
def sequence_connect():
    questions = config['questions']['sequence_connect']
    if request.method == 'POST':
        return render_template('sequence_connect.html',
                               questions=questions,
                               title="Sequence Connect")
    else:
        return 'There was a problem executing that task'


@app.route('/sequence-connect-question', methods=['POST', 'GET'])
def sequence_connect_question():
    questions = config['questions']['sequence_connect']
    if request.method == 'POST':
        q = request.form['question']
        q_selected = questions[q]
        config['questions']['sequence_connect'][q].update({'ignore': True})
        return render_template('sequence_connect_question.html',
                               questions=q_selected,
                               title="Sequence Connect")
    else:
        return 'There was a problem executing that task'


if __name__ == '__main__':
    app.run(debug=True)
