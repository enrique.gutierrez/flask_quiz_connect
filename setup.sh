#!/bin/bash
# setup script for development

python3 -m venv .venv && . .venv/bin/activate

pip install --upgrade pip
pip install -r requirements.txt
echo "Setup finished successfully"
